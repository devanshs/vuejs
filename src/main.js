import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
// import axios from "axios";

import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;
// axios.defaults.baseURL =
//  "http://api.openweathermap.org/data/2.5/weather?q={cityname}&?units=metric&appid={5299e83c9cf2fef510fb8ddaa3206ac8}"
new Vue({
  vuetify,
  render: (h) => h(App),
  router,
}).$mount("#app");
