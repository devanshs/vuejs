import Vue from "vue";
import VueRouter from "vue-router";
import SearchWeather from "@/components/SearchWeather";

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: "/search/:cityname",
      name: "SearchWeather",
      component: SearchWeather,
      props: true,
    },
  ],
  mode: "history",
});
